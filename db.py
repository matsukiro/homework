import sqlalchemy
from sqlalchemy.orm import Session

engine = sqlalchemy.create_engine('sqlite:///foo.db')
session = Session()

metadata = sqlalchemy.MetaData(bind=engine)
