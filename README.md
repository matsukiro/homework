Goal - run 2 services - 

WSGI service (pyramid) which runs restAPI service 
task worker (celery) which creates queue to receive tasks and process them.

For DB - use this http://docs.sqlalchemy.org/en/latest/core/engines.html#sqlite (DB in file) so it'll be easy for you to share the code with me. 

On the pyramid app, you should have 2 routes :

POST /upload

GET /results/{result_id}

The upload route should receive JSON data in the payload - in this format {1:[5,7,2],2:[8,3,2],3:[2,5,1]}
The upload API will save the every key- value to DB. to table named: client_data . the talbe columns are : id, raw_data, result
for the first key-value : id = 1, raw data = [5,7,2] , result = null 
Once saved to table, the API should send task to celery, to calculate the multiple of the items.
The celery service should receive the task, process and save to the db to the answer as result. 
So, when I'm using the second API - I'll call to /results/1
And it should return me the dict:  {"result":70}   # 5 * 7 * 2 = 70