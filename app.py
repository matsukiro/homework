from pyramid.renderers import JSON
from waitress import serve
from pyramid.config import Configurator


def upload(request):
    return request.json_body


def results(request):
    return int(request.matchdict['id'])


if __name__ == '__main__':
    with Configurator() as config:
        json_renderer = JSON()
        config.add_route('upload', '/upload')
        config.add_view(upload, route_name='upload', renderer="json")
        config.add_route('results', '/results/{id}')
        config.add_view(results, route_name='results', renderer="json")
        app = config.make_wsgi_app()
    serve(app, host='127.0.0.1', port=6543)
