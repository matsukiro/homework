import db

db.sqlalchemy.Table('client_data', db.metadata,
                    db.sqlalchemy.Column('id', db.sqlalchemy.Integer, primary_key=True),
                    db.sqlalchemy.Column('raw_data', db.sqlalchemy.String),
                    db.sqlalchemy.Column('result', db.sqlalchemy.Integer),
                    )

db.metadata.create_all(db.engine)
